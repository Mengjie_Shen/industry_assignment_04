package ictgradschool.industry.assignment04;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that allows you to pick a random word from a text file.
 */
public class RandomWords {

    private List<String> wordsList;

    /**
     * Creates the word generator by loading words.txt and adding all words in the file to wordList.
     */
    //Task One 1: Constructor - populate the wordsList list with the contents of the provided words.txt file
    public RandomWords() {

        this.wordsList = new ArrayList<>();

        File myFile = new File("words.txt");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(myFile))) {
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                String word = line.trim();
                this.wordsList.add(word);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Gets a random word from the set.
     *
     * @return a word
     */
    //Task One 2: method which returns a random word in the list
    public String getRandomWord() {
        return wordsList.get((int)(Math.random() * wordsList.size()));
    }

}
