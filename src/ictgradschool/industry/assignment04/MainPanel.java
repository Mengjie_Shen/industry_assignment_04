package ictgradschool.industry.assignment04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The main JPanel for Hangman, which contains all other GUI controls.
 */
public class MainPanel extends JPanel implements HangmanGameListener {

    private JTextField txtGuess;
    private JLabel lblRevealedWord;
    private JLabel lblStatus;
    private HangmanPanel hangmanPanel;
    private JButton btnGuess;

    private final int DELAY = 500;

    public MainPanel() {

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{1.0};
        gridBagLayout.rowWeights = new double[]{0, 0, 1.0, 0};
        setLayout(gridBagLayout);

        JPanel pnlRevealedWord = new JPanel();
        GridBagConstraints gbc_pnlGuessing = new GridBagConstraints();
        gbc_pnlGuessing.insets = new Insets(0, 0, 5, 0);
        gbc_pnlGuessing.gridx = 0;
        gbc_pnlGuessing.gridy = 0;
        add(pnlRevealedWord, gbc_pnlGuessing);

        JLabel lblYouAreGuessing = new JLabel("You are guessing: ");
        pnlRevealedWord.add(lblYouAreGuessing);

        lblRevealedWord = new JLabel("_ _ _ _ ");
        pnlRevealedWord.add(lblRevealedWord);

        lblStatus = new JLabel("You have 0 guesses remaining.");
        GridBagConstraints gbc_lblStatus = new GridBagConstraints();
        gbc_lblStatus.insets = new Insets(0, 0, 5, 0);
        gbc_lblStatus.gridx = 0;
        gbc_lblStatus.gridy = 1;
        add(lblStatus, gbc_lblStatus);

        //Task Three 1: Create HangmanPanel and enter new guesses
        hangmanPanel = new HangmanPanel();
        HangmanGame hangmanGame = hangmanPanel.getHangmanGame();

        GridBagConstraints gbc_hangmanPanel = new GridBagConstraints();
        gbc_hangmanPanel.insets = new Insets(0, 0, 5, 0);
        gbc_hangmanPanel.gridx = 0;
        gbc_hangmanPanel.gridy = 2;
        add(hangmanPanel, gbc_hangmanPanel);

        JPanel pnlControls = new JPanel();
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 3;
        add(pnlControls, gbc_panel);

        JLabel lblEnterYourGuess = new JLabel("Enter your guess: ");
        pnlControls.add(lblEnterYourGuess);

        txtGuess = new JTextField();
        pnlControls.add(txtGuess);
        txtGuess.setColumns(20);

        btnGuess = new JButton("Guess!");
        pnlControls.add(btnGuess);


        //add an event handler to the “guess” button which will call an appropriate method in game logic class
        btnGuess.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //read the guess the user has typed as an argument and call an appropriate method in game logic class
                String userInput = txtGuess.getText();
                hangmanGame.newGuess(userInput);

                //Task Three 2: View the partially-revealed secret word
                String resultStr = hangmanGame.returnedWord();
                lblRevealedWord.setText(resultStr);

                //Task Three 4: See a “congratulations” message if they win
                if (hangmanGame.hasWon()) {

                    lblStatus.setText("Congratulations, you win!");

                    //disable the button if the game is over
                    btnGuess.setEnabled(false);
                    repaint();

                    //The happy face should blink on and off about once every second.
                    Timer timer = new Timer(DELAY, new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            hangmanGame.setBlink();
                            repaint();

                        }
                    });

                    timer.start();

                //Task Three 5: See a “too bad” message if they lose
                //which will additionally reveal the entire secret word to them.
                } else if (hangmanGame.hasLost()) {

                    lblStatus.setText("Too bad, you lost :( The word is " + hangmanGame.getSecretWord());

                    //disable the button if the game is over
                    btnGuess.setEnabled(false);

                    //The sad face should blink on and off about once every second.
                    Timer timer = new Timer(DELAY, new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            hangmanGame.setBlink();
                            repaint();
                        }
                    });

                    timer.start();

                } else {
                    ////Task Three 3: View the number of remaining incorrect guesses
                    lblStatus.setText("You have " + hangmanGame.getIncorrectGuessRemaining() + " guesses remaining.");
                }


            }
        });

        //set observer on game logic
        HangmanGameListener hangmanGameListener = new HangmanGameListener() {
            @Override
            public void userGuessOnce(HangmanGame hangmanGame) {
                hangmanPanel.repaint();

            }
        };

        hangmanGame.addHangmanGameListener(hangmanGameListener);

    }


    @Override
    public void userGuessOnce(HangmanGame hangmanGame) {
        this.hangmanPanel.repaint();
    }

}
