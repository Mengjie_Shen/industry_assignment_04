package ictgradschool.industry.assignment04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Mengjie
 * Date : 2018/1/13
 * Time : 14:28
 **/
public class HangmanGame {

    private String secretWord;
    private Character[] guess;
    private int incorrectGuessRemaining;
    private int incorrect;
    private List<HangmanGameListener> listeners;
    private boolean blink = false;


    public HangmanGame() {

        //Task Two 1: Initialize a new game with a secret word from the RandomWords class.
        RandomWords randomWords = new RandomWords();

        //Task Two 2: Store the secret.
        this.secretWord = randomWords.getRandomWord();

        //Task Two 3: Keep track of which characters in the secret word the user has already correctly guessed.
        this.guess = new Character[secretWord.length()];

        //Task Two 4: Keep track of how many incorrect guesses the user has remaining before they lose.
        //This number should start at 8.
        this.incorrectGuessRemaining = 8;
        this.incorrect = secretWord.length();
        this.listeners = new ArrayList<HangmanGameListener>();

    }

    //Task Two 5: method should update your record of which characters have been correctly guessed if the guess was correct.
    //Or decrease the number of remaining incorrect guesses if the guess was incorrect.
    public void newGuess(String s) {

        //if user input contains only one character
        if (s.length() == 1) {
            boolean hasSingleMatch = false;

            //loop through the secret word and compare every character with s
            for (int i = 0; i < this.secretWord.length(); i++) {
                //if guess[i] is not null(which means this character has been revealed in previous guesses
                if (guess[i] != null && guess[i] == s.charAt(0)) {
                    continue;
                }

                //if certain character in secret word matches s, assign s to guess and decrease incorrect by 1
                if (Character.toString(this.secretWord.charAt(i)).equals(s)) {
                    this.guess[i] = this.secretWord.charAt(i);
                    this.incorrect--;
                    hasSingleMatch = true;

                }
            }

            //if this guess has no match, decrease incorrectGuessRemaining by i
            if (!hasSingleMatch) {
                this.incorrectGuessRemaining--;
            }

        //if user input is a whole word
        }else {
            //If input guess has the length of the secret word
            if (s.length() == this.secretWord.length()) {
                boolean hasAllMatch = true;

                //loop through the secret word and compare every character with s
                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == this.secretWord.charAt(i) && this.guess[i] == null) {
                        this.guess[i] = this.secretWord.charAt(i);
                        this.incorrect--;
                    }else {
                        hasAllMatch = false;
                    }
                }

                //If the user guessed a whole word that’s not the secret word, decrease incorrectGuessRemaining by i
                if (!hasAllMatch) {
                    this.incorrectGuessRemaining--;
                }

            //If the user guessed a whole word that doesn't even match the length of the secret word,
            }else {
                this.incorrectGuessRemaining--;
            }
        }

        //add listeners to hangmanGame
        //if hangmanGame changes, notify observers
        for(HangmanGameListener listener : listeners) {
            listener.userGuessOnce(this);
        }


    }

    //Task Two 6: method determining if the user has won (i.e. all characters in the secret word have been correctly guessed).
    public boolean hasWon() {
        if (this.incorrect == 0) {
            return true;
        }
        return false;
    }

    //Task Two 7: method determining if the user has lost (i.e. they have zero incorrect guesses remaining).
    public boolean hasLost() {
        if (this.incorrectGuessRemaining == 0) {
            return true;
        }
        return false;
    }

    //Task Two 8: method which returns the partially-revealed secret word, according to the guesses the user has already made.
    public String returnedWord() {
        String returnStr = "";
        for (int i = 0; i < guess.length; i++) {
            if (guess[i] == null) {
                returnStr += "_ ";
            }else {
                returnStr += (Character.toString(guess[i]));
            }
        }
        return returnStr;
    }

    //methods below are getters and setters
    public String getSecretWord() {
        return this.secretWord;
    }

    public int getIncorrectGuessRemaining() {
        return this.incorrectGuessRemaining;
    }


    public boolean getBlink() {
        return this.blink;
    }

    public void setBlink() {
        this.blink = !blink;
    }
    //methods above are getters and setters

    //method to add listener to hangmanGame's listeners
    public void addHangmanGameListener(HangmanGameListener listener) {
        this.listeners.add(listener);
    }

}
