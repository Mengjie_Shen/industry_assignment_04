package ictgradschool.industry.assignment04;

import javax.swing.*;
import java.awt.*;

/**
 * A panel which displays the graphics for a hangman game.
 */
public class HangmanPanel extends JPanel {

    private HangmanGame hangmanGame;


    public HangmanPanel() {
        setPreferredSize(new Dimension(200, 200));
        this.hangmanGame = new HangmanGame();
    }

    /**Draws the hangman */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        // Outline (should always be drawn)
        g.drawRect(0, 0, 199, 199);

        //Allow the HangmanPanel to know how many incorrect guesses remaining the user has.
        int incorrectGuessRemaining = this.hangmanGame.getIncorrectGuessRemaining();

        // Gallows
        if (incorrectGuessRemaining <= 7) {
            drawGallows(g);
        }
        // Head
        if (incorrectGuessRemaining <= 6) {
            drawHead(g);
        }
        // Body
        if (incorrectGuessRemaining <= 5) {
            drawBody(g);
        }
        // Arm 1
        if (incorrectGuessRemaining <= 4) {
            drawArm1(g);
        }
        // Arm 2
        if (incorrectGuessRemaining <= 3) {
            drawArm2(g);
        }
        // Leg 1
        if (incorrectGuessRemaining <= 2) {
            drawLeg1(g);
        }
        // Leg 2
        if (incorrectGuessRemaining <= 1) {
            drawLeg2(g);
        }
        // Noose
        if (incorrectGuessRemaining == 0) {
            drawNoose(g);
        }
        //When the user wins, an animation should start playing.
        //The entire human body (i.e. head, body, arms & legs) should be displayed, along with the “happy” face.
        if (this.hangmanGame.hasWon()) {
            drawMan(g);
            hideGallowsAndNoose(g);
            if (this.hangmanGame.getBlink()) {
                g.setColor(Color.black);
                drawWinFace(g);
            }else{
                g.setColor(getBackground());
                drawWinFace(g);
            }
        //When the user loses, an animation should play showing the entire body
        //(plus gallows and noose since they’ve lost), and a “sad” face.
        }else if (this.hangmanGame.hasLost()) {
            if (this.hangmanGame.getBlink()) {
                g.setColor(Color.black);
                drawLoseFace(g);
            }else{
                g.setColor(getBackground());
                drawLoseFace(g);
            }
        }

    }

    public HangmanGame getHangmanGame() {
        return this.hangmanGame;
    }

    public void drawGallows(Graphics g) {
        g.drawLine(150, 20, 150, 199);
        g.drawLine(50, 20, 150, 20);
    }

    public void drawHead(Graphics g) {
        g.drawOval(30, 30, 40, 40);
    }

    public void drawBody(Graphics g) {
        g.drawLine(50, 70, 50, 150);
    }

    public void drawArm1(Graphics g) {
        g.drawLine(50, 80, 20, 110);
    }

    public void drawArm2(Graphics g) {
        g.drawLine(50, 80, 80, 110);
    }

    public void drawLeg1(Graphics g) {
        g.drawLine(50, 150, 20, 180);
    }

    public void drawLeg2(Graphics g) {
        g.drawLine(50, 150, 80, 180);
    }

    public void drawNoose(Graphics g) {
        g.drawLine(50, 20, 50, 30);
    }

    //put six parts into one method
    public void drawMan(Graphics g) {
        drawHead(g);
        drawBody(g);
        drawArm1(g);
        drawArm2(g);
        drawLeg1(g);
        drawLeg2(g);
    }

    //method to draw smile face
    public void drawWinFace(Graphics g) {
        //Eyes
        g.drawLine(35, 50, 40, 40);
        g.drawLine(40, 40, 45, 50);
        g.drawLine(55, 50, 60, 40);
        g.drawLine(60, 40, 65, 50);
        //Mouth
        g.drawLine(40, 60, 60, 60);
    }

    //method to hide gallows and noose
    public void hideGallowsAndNoose(Graphics g) {
        g.setColor(getBackground());
        g.drawLine(150, 20, 150, 199);
        g.drawLine(50, 20, 150, 20);
        g.drawLine(50, 20, 50, 30);
    }

    //method to draw sad face
    public void drawLoseFace(Graphics g) {
        //Eyes
        g.drawLine(35, 50, 45, 40);
        g.drawLine(35, 40, 45, 50);
        g.drawLine(55, 50, 65, 40);
        g.drawLine(55, 40, 65, 50);
        //Mouth
        g.drawLine(40, 60, 60, 60);
    }


}
