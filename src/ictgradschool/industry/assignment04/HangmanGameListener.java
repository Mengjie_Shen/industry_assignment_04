package ictgradschool.industry.assignment04;

/**
 * Created by Mengjie
 * Date : 2018/1/13
 * Time : 14:53
 **/
public interface HangmanGameListener {
    void userGuessOnce(HangmanGame hangmanGame);
}
